# De l'importance VITALE d'un contrôle politique de la monnaie

Article à visée pédagogique, d'éducation et de mobilisation populaires, sur les principes d'une politique monétaire véritablement au service des citoyens.  

Texte sous licence [GPL version 3](http://www.gnu.org/licenses/gpl.html).

Les icônes utilisées pour les graphiques sont sous licence Creative Commons (utilisation personnelle) avec attribution : merci à [Freepik](https://www.flaticon.com/authors/freepik), [Smashicons](https://www.flaticon.com/authors/smashicons), [Dinosoft labs](https://www.flaticon.com/authors/dinosoftlabs), [Creaticca creative agency](https://www.flaticon.com/authors/creaticca-creative-agency) et [Monkik](https://www.flaticon.com/authors/monkik).


En espérant que ce qui est déposé ici suscitera des développements plus importants.



[Introduction](https://monnaie-politique.info/)

[I - La monnaie au service de l'économie physique](https://monnaie-politique.info/monnaie-economie-physique/)

[II - La monnaie dans le système actuel](https://monnaie-politique.info/le-systeme-actuel/)

[III - Pistes pour changer les institutions](https://monnaie-politique.info/changer-les-institutions/)

[Version pdf](http://monnaie-politique/wp-content/uploads/2020/02/De-limportance-VITALE-dun-contr%C3%B4le-politique-de-la-monnaie.pdf)

