
*********************************
La monnaie dans le système actuel
*********************************

Pour mieux comprendre le fonctionnement du système monétaire actuel,
nous allons devoir revenir sur une simplification faite au chapitre
précédent. Nous n’avons parlé que de *masse* monétaire, or cela ne
suffit pas à rendre compte du lien entre la monnaie et l’économie
physique.

Revenons à notre « marché » : au début de la journée chaque participant
dispose à la fois de biens à vendre et d’un budget pour ses achats. Nous
pouvons supposer que chaque bien ne sera vendu qu’une fois, du moins si
l’acheteur se le procure pour son propre usage (et non dans l’idée de le
revendre plus cher). Par contre, rien ne nous dit qu’un billet ne va
être utilisé qu’une seule fois au cours de la journée. Lorsqu’il passe
dans les poches d’un vendeur, celui-ci peut s’en servir pour acheter la
production d’un autre, et ainsi de suite. Certains billets vont circuler
plus vite, d’autres moins : nous disons alors que la monnaie a une
*vitesse de circulation*.

| Cette vitesse est aussi importante que la masse monétaire : pour une
  quantité de biens échangés égale, si la monnaie se mettait à circuler
  deux fois plus vite, cela aurait le même impact sur les prix que si la
  masse monétaire doublait. Juger de la circulation monétaire uniquement
  par une quantité de billets serait donc comme essayer de mesurer le
  débit d’un fleuve en litres : or un cours d’eau large mais lent n’aura
  pas un plus grand débit qu’un cours d’eau plus étroit, mais beaucoup
  plus rapide ; l’important étant la quantité d’eau *écoulée sur une
  période*. En sciences physiques, c’est ce que l’on appelle un *flux.*
  Ainsi pour éviter que le lien entre la monnaie et l’activité
  productive se distorde, il ne s’agit plus d’accorder une quantité de
  bien physiques avec une quantité de monnaie, mais d’accorder des
  flux : le flux de biens et services échangeables (nombre de biens
  produits sur une période) avec le flux monétaire. (nombre de
  *transactions* sur la période)
| Et dans la réalité… c’est loin d’être facile. La vitesse de
  circulation monétaire évolue en permanence. Les citoyens peuvent
  décider à tout moment de garder leur argent au chaud, ou de le
  dépenser immédiatement, et sans tenir compte des aléas de la
  production. *Anticiper ces mouvements et y faire face relève d’un
  véritable travail d’ingénierie économique*. Le risque de déflation (en
  cas d’épargne) ou d’inflation (en cas de dépense soudaine) est
  permanent, notamment dans le cas d’un mouvement collectif, et sans
  parler d’acteurs malveillants tentant de tirer profit de ce type de
  déstabilisation.

#. Les banques

De nos jours, la régulation de la circulation monétaire se fait
principalement par l’intermédiaire des établissements de dépôts et
crédits, communément appelés « les banques ». Lorsque vous laissez
dormir votre argent sur votre compte de dépôt, celui-ci est prêté à
quelqu’un d’autre dans l’intervalle, et il continue ainsi à circuler.

Ce procédé est assimilable à de la création monétaire pure et simple ;
en effet augmenter la vitesse de circulation de la monnaie de cette
manière aura le même impact sur le flux des transactions que d’augmenter
la masse monétaire - de créer de la monnaie. Il permet aussi de faire
d’une pierre deux coups : maintenir un flux monétaire en adéquation avec
le flux de biens échangeables, et rendre disponible des fonds pour des
projets nouveaux. Dans l’idéal, le métier de banquier peut même servir à
transformer de l’épargne de court terme en prêts à long terme, et
permettre ainsi de libérer des moyens pour des projets à priori
inabordables. La seule contrainte est que pour qu’une somme d’argent
puisse sortir de la banque, elle doit y être rentrée au préalable :
comme toute entreprise, la banque est considérée en faillite si ses
fonds propres ne permettent plus d’honorer ses engagements.

|image0|

Les établissements de dépôts et crédits ont donc bien un rôle positif à
jouer dans l’économie. Mais ils ont en même temps un énorme pouvoir, une
énorme responsabilité. Car la banque est libre d’ouvrir une ligne de
crédit à qui elle veut et pour le montant qu’elle veut, sa seule
contrainte étant de disposer des fonds suffisants en cas de retrait.
C’est ce que l’on appelle le « système à réserves fractionnaires ». Et
lorsque les paiements sont dématérialisés comme c’est le cas
actuellement, l’argent ne sort de la banque que pour rentrer dans une
autre, et dans de nombreux cas il ne fait que passer d’un compte à un
autre au sein du même établissement. À partir du moment où les banques
s’entendent entre elles pour se prêter les unes aux autres, (et c’est en
général le cas) la vitesse de circulation de la monnaie peut donc
techniquement être multipliée par dix, voire plus.  [1]_ Une erreur
d’estimation, une négligence ou un comportement malveillant pourrait
donc conduire à désaccorder complètement le flux monétaire et l’économie
physique.

Sans compter que ces opérations de dépôts et crédits ne sont pas leurs
seules sources de revenus. La plupart des pays ont adopté le modèle de
la « Banque Universelle », qui peut vendre toutes sortes de produits
comme des conseils en gestion, des assurances, les données personnelles
de leurs clients, ou même des services de téléphonie. Et notamment, si
elles ont si mauvaise réputation aujourd’hui c’est qu’elles pratiquent
la spéculation financière : pour faire simple, elles achètent et vendent
des biens dans l’unique but de faire un bénéfice lors d’une variation de
prix, et dans le même temps achètent et vendent des assurances pour se
prémunir contre lesdites variations. Elles disposent ainsi d’un panel
d’instruments financiers complexes leur permettant de s’enrichir lorsque
les prix montent, et de s’enrichir lorsque les prix baissent :
indépendamment, donc, des conséquences pour les producteurs. Là aussi la
responsabilité est grande, car les montants engagés font qu’elles
peuvent provoquer *elles-mêmes* des hausses et des baisses des prix, et
pas toujours dans le sens d’une plus grande stabilité. Par ailleurs,
leurs « paris » spéculatifs se font souvent avec un effet de levier : la
banque peut multiplier sa mise de départ par deux ou plus si elle
« gagne », mais peut aussi perdre au moins dix fois sa mise. Et dans le
cas d’une erreur d’estimation c’est tout l’établissement qui est menacé
de faillite… en évaporant au passage l’argent des épargnants.

2. L’institut d’émission « indépendant »

Que se passe-t-il, donc, lorsque la banque n’a pas assez d’argent dans
ses coffres pour honorer les retraits de ses clients ? C’est là
qu’intervient l’Institut d’Émission qui est chargé de créer l’argent et
de le mettre en circulation. En général il est toujours disposé à prêter
aux banques lorsque celles-ci ont besoin d’un peu d’argent frais,
moyennant un taux d’intérêt qui évolue selon les circonstances. Ces
dernières peuvent ainsi se refinancer en cas de coup dur, que ce soit
lorsqu’un ou plusieurs débiteurs ne peuvent plus tenir leurs
engagements, ou bien lorsqu’une opération spéculative a mal tournée.
C’est ainsi qu'à la suite de la crise de 2008, la Réserve fédérale
américaine a prêté aux banques la rondelette somme de mille milliards de
dollars en 1 an (soit 5% de la production annuelle du pays) sous forme
d’achats de titres financiers, pour éviter que les spéculations sur
l’immobilier américain ne déclenchent la faillite de l’ensemble du
système bancaire international.

Tel est, en substance, le fonctionnement du système monétaire actuel.
Avec quelques variations selon la zone géographique, mais globalement le
même schéma : des banques qui gèrent l’argent en circulation, et un
Institut d’Émission qui prête aux banques. Sauf exception, l’Institut
d’Émission n’intervient qu’\ *indirectement *: en Union Européenne où
sont écrites ces lignes, il lui est interdit d’accorder des prêts à
d’autres organismes que les banques privées ; la stabilité des prix dont
nous avons parlé plus haut fait bien partie de ses prérogatives, mais
cela s’arrête là ; l'Institut d'Émission n’a aucun objectif en termes de
direction à donner au travail humain. Même chose en Angleterre, où le
seul objectif officiel est cette stabilité des prix. Aux États-Unis
c’est un peu différent, l’Institut d’Émission ayant aussi la mission de
tendre vers un « taux d’emploi maximum » et des « taux d’intérêts à long
terme », ce qu’il doit chercher indirectement à travers une « croissance
des agrégats monétaires et de la quantité de crédit ». [2]_ Et ces trois
organismes, respectivement la « Banque Centrale Européenne », la
« Banque d’Angleterre » et la « Réserve Fédérale Américaine » sont
*indépendants*, à savoir qu’ils opèrent sans recevoir la moindre
consigne des élus et des instances démocratiques. Il en est de même pour
les banques centrales des 15 premières puissances mondiales, à
l'exception de la Chine et de l'Inde.  [3]_

3. Les conséquences

Les banques, donc, interviennent *directement* et sont en première ligne
de la création monétaire : ce sont elles qui décident ce qui sera fait
de l’argent, des orientations à donner à la circulation monétaire, de
*qui* disposera de lignes de crédit et pour quel projet. Et quand nous
disons « qui », cela comprend les citoyens, mais aussi les organismes
publics… et souvent même les États, qui n’ont pas le droit d’emprunter à
leur propre Institut d’Émission. Les montants ne sont pas anodins : en
France en 2017, 720 milliards de nouveaux crédits sont octroyés par les
banques privées soit plus de deux fois le budget du gouvernement, sans
même parler des prêts sous forme de titres financiers ; le montant total
des crédits en cours pour cette année est même égal au PIB du pays, 2200
milliards d’euros ;

Comme nous l’avons dit au chapitre précédent, la création monétaire est
l’instrument qui permet de donner une orientation à l’activité humaine,
que ce soit en terme quantitatif, qualitatif, ou pour ouvrir de
nouvelles voies suite à un changement technique majeur. Lorsque cet
instrument est l’apanage exclusif d’établissements à but lucratif (et
majoritairement privés), de deux choses l’une : soit il y a une seule
bonne manière de gagner de l’argent en octroyant des crédits, et dans
quel cas elle coïncide nécessairement avec l’intérêt général, avec
l’évolution légitime de l’activité humaine ; Soit c’est une tâche qui
implique des choix ayant différentes répercussions à long terme, dans
quel cas les établissements bancaires – et leurs propriétaires - sont
rémunérés pour mettre en œuvre leur propre politique.

Nous y reviendrons. Néanmoins à ce stade nous ne pouvons que vous
encourager, lorsque vous choisissez votre banque, à valider que sa
stratégie et ses valeurs correspondent bien à votre vision du monde.

Mais surtout, il manque quelque chose. Nous avons vu au chapitre
précédent que dans une société où la population augmente (de fait), le
maintien du niveau de vie pourrait être compromis si la circulation
monétaire n'augmente pas en proportion. Qu’à l’inverse un système où la
circulation monétaire est constante (à prix constants) correspond à une
société où la population et le niveau de vie restent indéfiniment les
mêmes. Dans une société comme la nôtre où le maintien de la production
va demander beaucoup moins de travail, sans un accroissement dirigé de
la circulation monétaire, ceux qui ne participeront plus à cette
production pourront crever - avec la maigre consolation de ne pas avoir
surpeuplé la planète. Or, dans le système actuel, *les Instituts
d’Émissions ne créent pas de monnaie de manière permanente.* Comme nous
l’avons mentionné c’est même mal vu, dans l’idée que « faire marcher la
planche à billet c’est inflationniste »\ *.* Certes de l’argent est
injecté dans le système lorsque l’Institut d’Émission prête à une banque
privée, ou plutôt lorsque celle-ci accorde de nouveaux crédits à ses
clients. Certes l’augmentation de la vitesse de circulation opérée par
les banques est équivalente à de la création monétaire. Mais cette
monnaie est temporaire, étant donné qu’elle est retirée de la
circulation au moment où le prêt est remboursé. Le maintien d’un certain
niveau de circulation monétaire nécessite donc que de nouvelles lignes
de crédits soient sans cesse accordées, qu’il y en ait sans cesse de
nouvelles pour soutenir la croissance physique, mais aussi pour
remplacer les anciennes à mesure que celle-ci arrivent à échéance. Cela
nous amène à faire deux constatations importantes : la première est que
dans ce système, *le taux d’intérêt auquel sont accordés ces prêts
correspond de facto à une ponction sur la production mondiale.* La
seconde est que cela implique une perpétuelle fuite en avant. Les
banques doivent trouver en permanence de nouveaux emprunteurs – donc
créer de nouvelles dettes – afin d’éviter une contraction monétaire qui
tirerait brutalement en arrière l'économie réelle [4]_. Pour ce système
il est donc vital de créer de nouvelles tendances, qu’elles soient liées
ou non à une évolution légitime de l’activité économique. Et au passage,
de trouver les moyens de faire tenir une pyramide de crédit de plus en
plus grosse, donc de moins en moins stable.

À ce sujet, les 90 000 milliards de dollars chiffrés par la Banque
Mondiale dans le but de réduire l’émission de CO\ :sub:`2`, seraient une
excellente « solution » pour maintenir un niveau de crédit suffisant
pour au moins une génération.

Histoire de rajouter de l'instabilité à l'instabilité, et pour finir
notre tour d'horizon, il reste à parler de la manière dont les échanges
se font entre les différentes zones monétaires. Il ne vous a peut-être
pas échappé que les monnaies sont parfois déséquilibrées, au point qu'un
citoyen de la zone euro voyageant dans un pays étrangers a son pouvoir
d'achat multiplié par 3 ou 4. En effet rien ne garantit, dans le système
actuel, que la valeur relative de deux monnaies soient équilibrée en
fonction des deux flux de biens physiques auxquelles elles
correspondent: car depuis quarante ans *les monnaies sont échangées
selon le principe de l'offre et de la demande,* au même titre que les
autres biens issus de la production de l'homme. Et cela pose quelques
problèmes.

Imaginez que souhaitiez importer un produit depuis les États-Unis. Pour
acheter ce produit vous allez devoir convertir votre monnaie dans celle
du pays, on dit alors que vous souhaitez « acheter » du dollar. Ce
faisant, vous augmentez la demande de dollars, et il n'y a peut-être pas
d'américains souhaitant « acheter votre monnaie » pour la compenser.
Dans quel cas le « prix » que vous allez devoir payer (la quantité de
votre propre monnaie que vous allez devoir fournir en échange) va
augmenter. C’est un peu comme si on jaugeait les zones économiques entre
elles : s’il y a beaucoup plus de gens voulant *sortir* de votre zone
monétaire que de gens voulant y *rentrer*, alors elle sera comme
dépréciée ; tout ce qui sera produit chez vous sera moins cher que la
production équivalente dans les zones monétaires plus demandées.

On pourrait croire que ce genre de déséquilibre ne tiendrait pas
longtemps, que les étrangers ne tarderont pas à s’apercevoir qu’il leur
est très favorable d’acheter leurs produits à moindre prix chez vous, et
qu’ils regonfleront ainsi le « cours » (le prix) de votre monnaie. Mais
la réalité est plus subtile : car il existe des techniques de
spéculation permettant de s’enrichir en faisant baisser votre monnaie,
d’autant plus faciles à mener que la zone monétaire est petite et que
les fonds engagés par le spéculateur sont importants. On dit alors
officiellement que votre monnaie est « attaquée »...

Ainsi, si votre monnaie est jugée trop vulnérable, les épargnants et les
entreprises préféreront convertir leurs fonds dans une monnaie plus
stable, plus « forte », qui s’en trouvera ainsi surévaluée [5]_.

Ces attaques sur la monnaie ne sont pas théoriques. Les plus connues
sont sans doute celle de la Livre Sterling en 1992, ayant conduit
l’Angleterre à sortir du système monétaire européen, et celle du Bath
Thaïlandais en 1997 dont les dommages, selon le ministre chinois des
affaires étrangères de l'époque, ont été similaires à ceux d’une guerre.
 [6]_ On parle beaucoup moins de celle menée depuis les années 60 par...
les banques de Londres, qui ont organisé le déplacement de masses de
dollars hors des États-Unis et ont finalement provoqué la dérégulation
complète du système financier international en 1973  [7]_. C’est à elles
que l’on doit la naissance de ce que l’on appelle le système
« offshore », un ensemble de paradis fiscaux promouvant le secret
bancaire à outrance, où il est impossible voire interdit de savoir qui
détient quels capitaux, rendant donc impossible le contrôle de votre
propre monnaie en dehors de vos frontières. Bref, un climat beaucoup
plus propice aux intrigues, aux rapports de forces et à la géopolitique
qu’au développement mutuel basé sur des principes physiques.

Le bilan de tout ceci ?

La stabilité des échanges mondiaux repose sur une pyramide de crédits et
de dettes qui peut à tout moment s'effondrer. Des sommes d'argent
énormes sont mises au service de la spéculation financière, accroissant
de manière spectaculaire les inégalités sociales et détournant le
travail humain de ce qui est réellement productif. A chaque crise, comme
en 2008, les conséquences sont réelles et tangibles pour les
populations : des conditions de vie qui deviennent plus dures, des
licenciements économiques, des foyers expropriés, des vies entravées ou
brisées. Des pays entiers se sont effondrés ou englués dans le
sous-développement suite à une déstabilisation monétaire, offrant un
terrain propice à l'apparition de conflits armés.

Et dans tout cela, nous observons une concentration de plus en plus
grande des pouvoirs économiques entre les mains de quelques entreprises
(notamment dans le secteur numérique avec les GAFAM), des populations
que se reconnaissent de moins en moins dans le futur qu'on leur propose
et en viennent parfois à souhaiter l'effondrement, une gestion des
ressources énergétiques (actuelles et à venir) qui semble n'offrir
d'autre alternative que la pénurie.

Certes, les Etats ne peuvent en être tenus comme *directement*
responsables car, nous l'avons vu, ils ne disposent pas des bons
leviers. Ils sont par ailleurs dépendants des marchés de capitaux
privés, sans qui ils ne peuvent financer leurs investissements et
soutenir leur monnaie. Leur faute est par contre d'avoir sciemment
transféré le contrôle de la création monétaire à d'autres acteurs : et
il semble bien que les dirigeants des Instituts d'Émission et les
banques, eux, n'ont pas su être à la hauteur de leurs responsabilités.
*La question se pose donc de continuer à leur déléguer entièrement cette
mission d'intérêt général.*

.. [1]
   a réglementation bancaire internationale « Bâle III » impose
   toutefois une limite à cette multiplication. Les banques sont ainsi
   tenues de maintenir un ratio minimum de 8% entre leurs capitaux
   propres et les prêts qu'elles accordent.

.. [2]
   Federal Reserve Act, 1978, Section
   2A :https://www.federalreserve.gov/aboutthefed/section2a.htm

.. [3]
   Pour bon nombre d'entre elles, l'indépendance de la Banque Centrale
   est récente et a été actée suite à une législation passée dans les
   années 90.

.. [4]
   Sans compter que l’augmentation du poids de la dette des entreprises
   peut conduire ces dernières à augmenter leurs prix, ce qui n’est ni
   plus ni moins qu’une forme d’inflation si la tendance est
   généralisée.

.. [5]
   L’effet est tellement important que le dollar est devenu monnaie de
   référence pour les échanges mondiaux, ce qui offre aux Etats-Unis un
   énorme avantage : les utilisateurs de cette monnaie sont soumis à la
   Loi américaine.

.. [6]
   « Full Text of Chinese FM’s Address at ARF » Xinhua, July 27, 1998 :
   «La crise financière est-asiatique s'est déclenchée de manière féroce
   et a causé de tels dommages qu'ils ne sont pas différents de ceux
   d'une guerre.»

.. [7]
   « Treasure Island » de Oliver Shaxson, notamment le chapitre 5.

.. |image0| image:: accorder-les-flux.png
   :width: 7.99306in
   :height: 7.04167in
