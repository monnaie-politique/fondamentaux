********************************************
La monnaie au service de l'économie physique
********************************************


*Peut-être vous êtes-vous déjà étonnés, parfois indignés, des situations
absurdes produites par « l’économie » : des marchandises qui font
plusieurs milliers de kilomètres au lieu d’être produites sur place, des
millions de chômeurs alors qu’il y a tant à faire, des milliards de
dollars injectés dans le système sans que cela change quoi que ce soit
pour la condition des citoyens ordinaires. On croirait de plus en plus
que le monde de l’argent a oublié le monde réel, sa nature physique. Et
quand on se la rappelle c’est avec l’intuition que ça ne peut pas
durer.*

Cet article a pour but de vous montrer à quel point le contrôle
politique de la monnaie est vital. Que vous souhaitiez œuvrer pour une
démocratie véritable, pour la justice sociale, pour prendre à bras le
corps la question écologique, ou si vous en avez simplement marre de
vivre dans un monde ou 80 % des richesses créées reviennent à 1 % de la
population mondiale, il est indispensable que vous preniez connaissance
de ce qui est développé ici.

Si vous êtes un simple citoyen, et estimez que votre rôle n’est pas de
faire de la politique mais juste d’en savoir le minimum pour voter,
alors cet article est aussi fait pour vous. En moins de temps qu’il n’en
faut pour finir une série Netflix, il vous montrera pourquoi la moindre
promesse électorale de type « rétablir la souveraineté nationale »,
« libérer les énergies créatrices », « construire un monde plus humain »
ou « en finir avec des inégalités criantes et inadmissibles » relève de
la mauvaise blague ou de l’escroquerie si elle n’a pas comme préalable
le contrôle politique de la monnaie,.

Nous allons donc parler ici d’argent. Que l’on soit bien clair :
l’argent ne « crée » rien, et ne créera jamais rien par lui-même. Il ne
se mange pas, ne soigne pas, n’embellit pas la personne qui le possède.
Pourtant la manière dont il est utilisé a un impact, car il est
inévitablement lié au monde physique. Dans une première partie nous
parlerons donc de la nature de ce lien et de comment en tirer le
meilleur parti. Puis nous nous pencherons sur le fonctionnement du
système actuel. Enfin nous donnerons une première idée de la manière
dont nos institutions doivent concrètement fonctionner, du moins si nous
voulons que l’argent soit au service des orientations politiques voulues
par les citoyens.

#. La monnaie au service de l’économie physique

   #. Tout salaire mérite travail

L’économie est d’abord physique. S’il est communément admis que la
fonction première de l’argent – si ce n’est la seule – est de nous
permettre d’échanger facilement des biens et des services, la
quasi-totalité de la production humaine est issue d’un *travail*. En
dehors des abstractions introduites par la notion de propriété (rentes,
brevets, etc.), c’est donc du travail que nous échangeons.  [1]_

En substance, le terme « Travail » tel qu’il est utilisé dans le domaine
économique n’est pas très différent de son homologue en science
physique : l’action d’une force sur un objet, provoquant son mouvement
ou sa transformation.

Le travail exercé par l’être humain (ci-après dénommé « travail
humain ») a toutefois quelque chose de plus qu’une force mécanique.
Contrairement aux autres forces physiques qui exercent toujours le même
effort et engendrent le même résultat, il change de nature et
s’améliore, au fur et à mesure que l’humain découvre les principes du
monde qui l’entoure. Il n’appartient donc pas seulement au domaine
mécanique, il dépend d’une idée ; de l’idée que l’on se fait du monde à
un instant donné. Il organise la matière d’une manière qui n’avait
jamais été vue auparavant : la Vie avait déjà transformé le visage de la
Terre, à travers le Travail Humain les idées façonnent le monde de
manière encore plus profonde, plus rapide, plus radicale… plus
dangereuse aussi, si l’on s’accroche à des illusions et des modes de
pensées erronés.  [2]_

Cette quête d’une meilleure compréhension du monde implique que les
êtres humains sortent de plus en plus de la survie, et disposent d’un
espace mental suffisant pour trouver des réponses à des questions de
plus en plus profondes.

Elle implique un dialogue, une recherche commune, elle implique de
regarder l’autre comme un égal et non comme une ressource de plus à
exploiter.

Elle implique, comme le disait Jaurès, des individus qui apprennent « à
dire ‘moi’ , non par les témérités de l’indiscipline ou de l’orgueil,
mais par la force de la vie intérieure. »

Dans tout cela, l’argent est plus qu’une simple valeur d’échange : c’est
un outil de transformation de l’environnement. Celui qui possède une
grande quantité d’argent peut disposer du travail d’autres êtres humains
en quantité équivalente, et l’orienter à son idée. Tant mieux si cette
idée est d’améliorer le Travail Humain, de parvenir à des conditions
sociales plus justes (ce qui va de pair) ou éventuellement à une
meilleure harmonie avec l’Univers, mais le contraire peut s’avérer
destructeur.

Comme nous allons le voir maintenant ce n’est pas juste une question
d’individus : la vraie question est comment le système monétaire est
organisé.

2. Le lien

Pour comprendre cela nous allons voir comment s’établit la relation
entre la monnaie et le bien échangeable (donc la quantité de travail
équivalente). Ce qu'on appelle communément le prix.

Cette question du prix a donné du fil à retordre à des générations de
chercheurs, et nous ne la traiterons pas en profondeur ici. Nous
considérons d’ailleurs qu’elle n’est pas l’alpha et l’oméga de
l’économie. Néanmoins nous devons poser quelques considérations
importantes.

En dehors de toute contrainte extérieure, le prix se fixe d’un commun
accord entre l’acheteur et le vendeur : il dépend de ce que le premier
peut (ou veut bien) y mettre, et de ce que le second est prêt à
accepter.

On pourrait penser que ce prix dépend de la quantité de travail
nécessaire à produire le bien échangé, incluant l’extraction des
ressources et la fabrication des outils nécessaires ; ce qu’on appellera
ici le « prix naturel » [3]_. C’est en général le cas : si un jour vous
voyez qu’un bien est vendu très cher alors qu’il est très facile à
produire, vous serez sans doute tentés d’en fabriquer vous aussi ;
quitte éventuellement à le vendre un peu moins cher pour être sûr de
trouver un acheteur. Et au contraire, si un bien est compliqué à
produire et ne peut trouver d’acheteur prêt à y mettre le prix, il est
très probable qu’il ne sera pas produit du tout.

On aimerait aussi que le prix prenne en compte la relative rareté des
« ressources », à savoir la matière brute nécessaire à la fabrication du
bien échangé. Mais il semble que cela ne se fasse qu’indirectement, à
travers le travail nécessaire se procurer cette ressource : le prix a
tendance à augmenter lorsqu’elle devient plus difficile à trouver et à
extraire, et à diminuer lorsque de nouveaux gisements ou modes
d’extractions sont découverts  [4]_. C’est là le drame des indicateurs
économiques ne tenant compte que des aspects monétaires de l’économie,
comme le PIB : on peut y lire des échanges et à travers eux une quantité
de travail, mais pas la véritable nature de la transformation physique
opérée par l’activité humaine. Un bon économiste doit donc être capable
d’aller au-delà de ces indicateurs monétaires s’il veut faire des
prévisions pertinentes ou promouvoir les bonnes stratégies.

En général, on considère que le prix d'un bien évolue selon le modèle de
« l'Offre et la Demande », qui fonctionne un peu comme un système
d'enchères : comme dans une salle de vente si la quantité de biens
disponible à la vente (l'offre) est inférieure au nombre d'acheteurs
souhaitant l'obtenir (la demande), alors on fera monter le prix jusqu'à
ce que des acheteurs se retirent ; dans le cas contraire où les vendeurs
sont moins nombreux que les acheteurs, on fera plutôt jouer à la baisse
; Et le « juste » prix sera trouvé au moment où il y aura le même nombre
d'acheteurs et de vendeurs. Avec quelques variations sur les modalités,
c'est ainsi que fonctionne la plupart des place financières mondiales.

Ceci étant dit, ce modèle de fixation des prix par la concurrence se
complique énormément lorsque l’acheteur et le vendeur se comportent
comme des *joueurs* ayant chacun une stratégie. Tout est alors possible[5]_ : achats/ventes massifs dans le but de tirer parti d'une variation
brutale des prix, étranglement des producteurs, formation de monopoles
mettant l’acheteur en état de dépendance, fausses annonces sur la
disponibilité ou l'épuisement d’une ressource, fixation des prix par les
états, mise en place d’accords, conglomérats, syndicats, grèves,
boycotts, embargos, guerres commerciales, guerre tout-court, etc.

Parmi tous ces facteurs, la gestion de la monnaie *en tant que telle*
joue un rôle important.

Pour comprendre cela, nous imaginons l’économie mondiale comme un
« marché ». Par exemple, une grande place au centre-ville où l’on vend
des fruits et légumes. À la fin d’une journée une certaine quantité de
production a été écoulée, une certaine quantité d’argent a été dépensée
par les acheteurs, et à partir de ces deux quantités on peut en déduire
le prix moyen des marchandises échangées.

| Imaginez maintenant que par un coup de baguette magique, nous
  doublions la quantité d’argent possédée par les acheteurs. Certains
  vont être tentés d’acheter beaucoup plus, quitte à épuiser les stocks
  disponibles. D’autres seront prêts à payer plus cher pour être sûrs
  d’avoir la meilleure qualité. Les producteurs quant à eux, s’ils sont
  aussi intelligents et cupides que dans la théorie économique
  classique, vont chercher à tirer profit de la situation. Certains vont
  réfléchir à augmenter leur volume pour le jour de marché suivant, et
  dans l’immédiat ils vont vendre plus cher. Et c’est ainsi qu’une
  hausse de la quantité de monnaie en circulation va créer une hausse
  correspondante des prix, en plus de susciter une hausse de l’activité.
| Le phénomène inverse se produit si nous doublons le volume de
  production. Quand bien même la demande pour cette production
  existerait, nombre d’acheteurs n’auraient pas dans leur portefeuille
  de quoi la procurer. Et surtout, *la quantité de monnaie nécessaire
  aux transactions n’existerait pas*\  [6]_. Une partie de la production
  ne pourra tout simplement pas être échangée, et si elle ne peut être
  stockée elle sera détruite… sauf si les producteurs décident de
  baisser leurs prix.

La situation est encore plus dramatique si nous divisons par deux la
quantité d’argent en circulation. Les producteurs vont devoir brader
leurs marchandises, certains d’entre eux feront peut-être faillite. En
plus d’avoir un impact sur les prix, la gestion monétaire influencera
donc directement l’activité humaine.

|image0|

Cet exemple a pour but d’illustrer l’existence d’un lien élastique entre
la production et la monnaie. Une augmentation de la circulation
monétaire sans augmentation égale de la production va générer une hausse
des prix, ce qu’on appelle « l’inflation ». De même, une baisse de la
circulation de la masse monétaire par rapport à la production (ou bien
une augmentation de la production sans contrepartie monétaire) va
générer une baisse des prix, ce qu’on appelle «la déflation ». [7]_

En général c’est plutôt une bonne idée de stabiliser les prix, c’est
d’ailleurs l’objectif premier de bon nombre d’« Instituts d’Émission »
(autrement appelés « Banques Centrales » ou « Banques Nationales ») à
travers le monde. Une entreprise quelle qu’elle soit fonctionnera
beaucoup mieux si elle n’a pas à vérifier en permanence le prix de ses
fournitures et de ses équipements. De même, un citoyen touchant une
somme d’argent en échange d’un bien ou de son travail, s’attend à ce que
la valeur de son argent ne baisse pas avec le temps ; sans quoi il sera
probablement furieux. Dans les deux cas, une instabilité des prix aura
un impact négatif sur l’activité humaine.

Dans ce cadre, nous souhaitons faire une constatation importante : un
système où la circulation monétaire est constante correspond à un monde
physique où le niveau de vie est toujours le même, pour une population
qui est toujours la même. Dans le cas où la population augmente, il
correspond à un monde où le pouvoir d'achat s'amenuise, où les
conditions de vie se dégradent. À contrario un monde où la qualité de
vie s’améliore, où la production grandit en quantité *et/ou en*
*qualité*, nécessite en contrepartie une augmentation de la circulation
monétaire.

Mais nous pouvons aller plus loin : sous certaines conditions, une
intervention dans le système monétaire peut servir à *provoquer* une
évolution délibérée de l’activité humaine. C’est ce que nous allons voir
maintenant.

3. Provoquer les changements

Imaginez que nous souhaitions réaliser l’un de ces projets
incontestablement utiles mais qu’on dit impossibles à financer ou même
« pharaoniques », comme la création de 500 000 logements neufs par an
sur le territoire national.

Si nous suivons ce qui a été dit précédemment sur le lien
monnaie-production, nous pouvons prévoir *qu’une fois le projet réalisé*
il faudrait s'assurer qu'une quantité de monnaie correspondante aux 500
000 logements a été mise en circulation, afin que ceux-ci puissent être
échangés dans de bonnes conditions et sans déformation des prix. Parmi
toutes les choses à penser pour mener à bien notre chantier, il faudra
donc à un moment donné *émettre de la monnaie*.

De cette considération en apparence purement abstraite, vient alors une
idée : est-il possible d’émettre *en amont* cette quantité de monnaie
afin de payer la construction du projet, sachant qu'\ *en aval*, une
fois le projet abouti, elle aura sa contrepartie physique ? La réponse
est oui, cela fonctionne, et *le procédé a été éprouvé lors du New Deal
de Roosevelt et de la reconstruction française d’après 1945*. Il est
tout-à-fait possible de créer de l’argent *à partir de rien* et de s’en
servir pour payer les salariés et les fournitures, du moment que les
richesses générées à la fin sont réelles. Même si cela peut paraître
choquant, c’est ainsi que l’on peut mener une politique de grands
travaux *à l’échelle d’un pays entier*, pour des montants qui seraient
autrement considérés comme prohibitifs.

Mais si c’était vraiment si simple, ne l’aurait-on pas fait depuis
longtemps ? Peut-on vraiment diriger l’économie de cette façon ? Pas
pour la plupart des experts actuels pour qui « faire marcher la planche
à billet c’est inflationniste ». Pour faire simple, c’est bien beau
d’imprimer des tonnes d’argent en faisant un pari sur l’avenir, mais en
attendant la hausse des prix résultante sera bien réelle, la confiance
dans la monnaie sera ébranlée, l’activité économique sera rendue
instable, et on aura de très gros problèmes à résoudre. Et dans tout
cela, l’aboutissement même du projet sera bien évidemment compromis.
Cela fait sens, et c’est l’argument principal pour justifier que la
planche à billet (autre nom des Institut d’Émission) soit gérée par des
experts, non des élus, et considérer que ce procédé est une mauvaise
solution miracle. Mais ce scénario est tout-à-fait évitable : pour
comprendre cela, et pour que vous puissiez accorder un minimum de
vraisemblance à ce procédé étonnant, nous allons devoir examiner un peu
plus en détail comment cela se passe.

L’erreur serait de penser qu’à l’instant où vous augmentez la masse
monétaire, l’ensemble des prix va augmenter *instantanément*. En réalité
cela se passe comme si vous versiez un seau d’eau dans une baignoire :
l’eau ne va pas monter uniformément, mais d’abord là où vous l’avez
versée, *au voisinage de là où l’action a été effectuée*. En clair, une
vague va se propager jusqu’au parois de la baignoire et revenir. On dit
que c’est un *phénomène ondulatoire,* dans notre cas se propageant non
pas de manière *continue* mais à chaque acte d’achat/vente ou
*«* transaction *».*

Plus précisément, l’argent émis pour financer la construction de nos
logements va d’abord servir à embaucher des employés et à acheter des
fournitures. C’est d’abord là qu’un phénomène de hausse des prix peut se
manifester, notamment si le surcroît de *demande* que nous occasionnons
– ou que nos employés occasionnent en dépensant leur salaire - ne peut
être compensé par une augmentation de l’\ *offre* des fournisseurs, et
met le marché sous *tension*. Mais ce n’est pas automatique : si le
fournisseur dispose de marges sur ces capacités de productions, ou s’il
dispose de stocks, la tension ressentie sera minimale. Dans le cas de
logiciels et autres biens immatériels, elle sera quasi-nulle. Mais
surtout, nous pouvons la minimiser si nous avons fait en sorte *au
préalable* que le fournisseur (ou ses concurrents) puisse augmenter ses
capacités de productions, en vue des nouvelles demandes que nous
introduisons dans l’activité économique. Reste alors à savoir comment
faire pour que cette augmentation génère le moins possible de tension et
d’inflation.

Concernant le risque d’inflation lié à nos employés, nous pouvons le
traiter de la même façon. Selon la manière dont ils utiliseront leur
salaire cela générera aussi un surcroît de demande dans divers secteurs.
Nous devons alors *anticiper* les tensions qui pourraient en résulter,
en nous assurant comme précédemment de la suffisance des capacités de
production. Par ailleurs une partie de leur salaire consacrée aux impôts
et cotisations aura un impact positif sur les services de l’État et la
Sécurité Sociale, et une autre partie du salaire sera sans doute mise de
côté, peut-être en vue de l'achat d’un logement - ce qui permettrait
dans notre cas de faire d’une pierre deux coups.

Nous déplaçons ainsi les tensions sur les prix, de proche en proche. Et
au fur-et-à-mesure que nous les déplaçons nous les *amortissons*.

Tel est, en substance, le type d’ingénierie économique à déployer pour
mettre la création monétaire au service d’une évolution *délibérée* de
l’activité humaine. Notez que le procédé est neutre : il peut être
utilisé aussi bien pour construire des logements, des voitures
électriques ou des satellites, que des centres commerciaux, des
chaudières au fioul… ou même des armes. Le critère principal étant que
la demande existe pour cette production.

A ce stade nous souhaitons faire plusieurs constatations.

La première est que le succès de notre opération, telle que nous
l’envisageons ici, demande des transformations du tissu productif, et
éventuellement des choix à faire en termes d'évolution de la production,
d'utilisation de telle ou telle méthode. *Il implique une stratégie
d’ensemble et une coordination entre les acteurs,* autrement dit un
plan\ *.* Les compétences socio-économiques permettant d’imaginer et de
déployer ce type de stratégie sont donc essentielles.

La seconde est que notre méthode n’est pleinement légitime que lorsque
de nombreux travailleurs sont inactifs, *que de nouvelles forces de
travail peuvent être introduites dans le circuit de l’activité*.

Par ailleurs, nous avons parlé de projet visant à accroître la
production ; le principe serait le même si notre projet visait non pas à
accroître la *quantité* de production mais sa *qualité*. De manière
générale, il est valable pour toute évolution auquel l'acheteur accorde
de la valeur : si par exemple l’acheteur est prêt à mettre le prix pour
des biens dont le mode de production est plus « écologique », plus
respectueux des êtres vivants, ou tout autre critère objectif ou
subjectif  [8]_ demandant un surcroît de travail, alors cette évolution
pourra être financée par de la création monétaire.

Les choses sont un peu différentes si ce que nous cherchons à développer
n’est pas la quantité ou même la qualité des biens échangés, mais la
*productivité du travail* même  [9]_\ *:* nous souhaitons devenir plus
efficaces dans notre besogne afin de travailler moins, gagner plus, ou
simplement avoir une meilleure vie\ *.* Que ce soit par l’introduction
de technologies ou de ressources nouvelles, par la montée en compétence
des travailleurs, par un effort de formation ou simplement par économie
d’échelle, cette transformation ne change pas nécessairement la quantité
ou la qualité de ce qui est produit : Elle n’implique donc pas
directement de contrepartie monétaire. Néanmoins elle libère du temps de
travail qui pourra être réinvesti dans une croissance quantitative ou
qualitative de l’activité, spontanément ou – le cas échéant – selon le
principe que nous venons d’énoncer. C’est tout l’enjeu de la révolution
numérique et robotique à laquelle nous sommes actuellement confrontés :
des millions de travailleurs vont être remplacés par des machines, et
risquent à très court terme d’être purement et simplement laissés sur la
touche. Mais la mise en disponibilité de la gigantesque force de travail
que cela représente nous permettrait aussi d’ouvrir de nouveaux
chantiers auxquels nous ne pouvions nous consacrer auparavant :
l’ouverture d’activités à forte valeur sociale comme les services à
l’enfance et aux personnes âgées ; un effort de recherche fondamentale
et d’ingénierie de pointe, permettant la découverte de nouvelles
ressources et l’amélioration de notre efficacité énergétique ; le
développement intérieur de l'individu à travers l’éducation, les arts,
la philosophie. Le financement par une création monétaire ex-nihilo,
mettant en mouvement ces forces productives et anticipant les nouvelles
richesses générées, est la voie étroite qui sépare le rêve du cauchemar.

Cela nous amène d’ailleurs à revisiter la notion de « croissance », bien
souvent utilisée pour désigner les travers d’une société de consommation
à la surproduction effrénée : de notre point de vue il y a croissance
dès lors qu’il y a plus de travail engagé dans l’activité humaine, et
donc nécessité d’une augmentation de la masse monétaire.

Nous vous demandons pour la suite d'avoir bien en tête cette définition
neutre de la « croissance », qui peut désigner aussi bien une croissance
*matérielle* comme la surproduction de biens de consommation, une
croissance *qualitative* comme l’implémentation de modes de productions
plus respectueux/écologiques, ou une croissance *spirituelle* avec le
développement d’activités immatérielles comme l’histoire, l’art, les
sciences et la philosophie, nourrissant la vie intérieure des citoyens
et apportant du sens.

Nous espérons dans cette partie vous avoir montré qu’il n’y a pas de
crise financière qui soit insoluble : l’essentiel étant de maintenir le
lien entre le monétaire et le physique, de savoir quelles nouvelles
activités nous souhaitons voir émerger, de créer des cycles vertueux où
nous apprenons à produire plus vite et mieux, de provoquer et
d’accompagner les grands changements de l’Histoire. Au-delà des
chiffres, la vraie question est de savoir comment nous travaillons,
quelles ressources nous utilisons, et comment nous voyons la prochaine
étape de l’aventure humaine – étape qui à coup sûr, ne ressemblera à
rien de ce qui a déjà été observé.

Il n’y a aucune limite monétaire à l’investissement. La seule limite
réside dans le nombre de travailleurs que nous pouvons engager, et des
équipements que nous pouvons leur procurer. C’est, comme cela a été
énoncé dans le premier Plan de modernisation et d’équipement de la
France, « une limite physique ».  [10]_

Ceci étant dit, vous êtes peut-être gênés à l’idée que l’on puisse créer
de l’argent, comme ça, juste parce que nous avons un beau projet, sans
avoir trimé dur pour le mériter ; après tout cela ressemble un peu à du
vol. À cela nous répondrons deux choses : d’une part, le critère qui
nous semble le plus important est que le pouvoir d’achat lié à une somme
d’argent doit rester constant avec le temps, et nous avons montré que
cela implique bien souvent des créations monétaires ex-nihilo. D’autre
part, cela existe déjà et cela se fait tous les jours... C’est ce que
nous allons voir maintenant.

.. [1]
   On peut d’ailleurs noter que lorsque l’on juge communément la
   légitimité des abstractions en question, c’est souvent par rapport à
   leur correspondance à un travail.

.. [2]
   Ce danger est d’autant plus grand que la majeure partie des êtres
   humains est exclue de ce processus de recherche et d’idées qui
   sous-tend l’amélioration du travail.

.. [3]
   La définition exacte du prix naturel par les économistes classiques
   comprend également la rente foncière et la rente du capital, que nous
   avons délibérément écarté ici.

.. [4]
   À titre d’exemple, le rapport d'information n° 105 (2005-2006) du
   Sénat étudiant les facteurs responsables de la hausse du prix du
   pétrole fait mention de la réduction des capacités de production,
   mais pas des réserves disponibles.

.. [5]
   En cela la Théorie des Jeux met clairement en valeur les lacunes de
   l’Économie de Marché.

.. [6]
   C’est d’autant plus vrai si nous considérons un marché global où
   s’échange l’ensemble de la production.

.. [7]
   Le lien est rendu plus complexe si nous prenons en compte les
   variations potentielles de vitesse de circulation de la monnaie,
   comme nous le mentionnerons plus loin.

.. [8]
   Nous sommes toutefois conscients que la reconnaissance d'une telle
   subjectivité de la valeur ouvre une boîte de Pandore, notamment en ce
   qu'elle peut engendrer des contradictions logiques.

.. [9]
   À travers cette notion nous ne parlons pas nécessairement d’une
   énième doctrine telle que le taylorisme visant à abrutir les être
   humains et à les transformer en robots pour être plus rapides. (ce
   qui d’ailleurs aurait des effets pas forcément positifs sur
   l’activité humaine prise dans son ensemble) Plus largement, elle
   touche à ce qui caractérise l'essence même du développement humain ;
   cette capacité à améliorer constamment sa manière de faire, fondée
   sur une compréhension croissante de l'univers et de ses principes,
   dont nous avons parlé plus haut. Lorsqu’on l’aborde dans toute sa
   profondeur, c'est une transformation qu’on peut difficilement réduire
   à une formule mathématique : ce serait comme tenter de mesurer si oui
   ou non, la pratique du violon était déterminante dans le travail
   scientifique d’Einstein, ou de savoir si la réédition des œuvres
   complètes de Confucius aura un impact sur la productivité globale de
   la Chine. Ce serait comme tenter de déterminer, de deux
   environnements culturels, lequel est le plus propice à développer
   l’imagination, la curiosité et autres qualités nécessaires à
   l’invention comme à la recherche fondamentale.

.. [10]
   | Voir le plan de modernisation et d’équipement de la France
     1947-1953, page 84, disponible sur le site de France Stratégie :
     https://www.strategie.gouv.fr/actualites/premier-plan-de-modernisation-dequipement
   | Il est à noter que la reconstruction de la France durant la période
     ne s’est pas faite sans une forte inflation, à tel point que selon
     MM Jean-Pierre Patat, directeur général honoraire de la Banque de
     France et Michel Lutfallat déclarèrent dans leur Histoire Monétaire
     du 20e siècle qu'elle fut « payée par la monnaie ». Cette inflation
     aurait pu toutefois être pire suite aux circonstances de
     l’après-guerre, et la contenir était l’un des objectifs du Plan.
     D’autre part elle semble avoir été jugée comme secondaire par
     l’Histoire, qui a plutôt retenu la rapidité de la reconstruction et
     la période d’élévation du niveau de vie qui s’en est suivi.

.. |image0| image:: lien-monnaie-physique.png
   :width: 4.32708in
   :height: 5.53611in
