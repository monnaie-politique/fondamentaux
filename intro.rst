*******************************************************
Pourquoi le contrôle politique de la monnaie est VITAL.
*******************************************************

*Ce texte introduit un article plus complet sur l'importance vitale d'un
contrôle politique de la monnaie. Il a pour but d'aider le lecteur à en
comprendre rapidement les enjeux, sans avoir nécessairement à se lancer
dans une étude plus longue (quoique non rébarbative !)*

S'il y a une question qui ne devrait pas échapper aux citoyens et aux
plateaux de télévision, c'est bien celle du contrôle politique de la
monnaie, et plus particulièrement de la *Création monétaire*. Non pas
qu'elle soit plus importante que le retour au plein emploi, le
financement des retraites, les inégalités sociales, la défense
extérieure du pays ou le sauvetage de la planète. C'est plutôt que sans
elle, même les gouvernements les plus honnêtes pourront difficilement y
faire quoi que ce soit de significatif.

| Vous le savez peut-être : des millions de dollars, d'euros, de yuan et
  autres monnaies sont créés chaque jour par les "Banques centrales" des
  différents pays du monde, que ce soit en imprimant des billets ou en
  créditant simplement un compte bancaire. Alors que le faux-monnayage
  est souvent considéré comme crime contre l'Etat et puni avec une
  extrême sévérité, il suffirait à ces institutions d'appuyer sur un
  bouton pour permettre à un acteur public ou privé de bénéficier
  instantanément de nouveaux moyens.
| Et ce n'est pas forcément un mal. C'est par une création monétaire
  massive que Roosevelt a pu opérer son "New Deal" en 1932 et résorber
  le chômage de masse. C'est aussi grâce à elle, en partie, que la
  France a pu organiser sa reconstruction et prospérer après la seconde
  guerre mondiale. Loin d'être de la "triche", ce procédé est légitime
  quand on regarde l'activité humaine dans son ensemble : les richesses
  générées par l'arrivée de nouveaux travailleurs ne peuvent être
  échangées dans de bonnes conditions si l'on n'augmente pas en
  conséquence la circulation monétaire. Ceci est vrai de manière
  générale pour tout projet visant à une croissance quantitative ou
  *qualitative* de la vie économique : si l'on veut perturber le moins
  possible le lien qui relie la monnaie au monde physique, tout
  changement dans l'un doit avoir sa contrepartie dans l'autre. Dans le
  cas contraire, cela constituerait un frein au développement des
  nouvelles activités tout en faisant varier à la baisse la valeur de
  l'argent.

La croissance économique *réelle* doit donc au minimum être *coordonnée*
avec une croissance *monétaire.* Après lecture vous verrez même qu'il
est tout-à-fait pertinent de financer l'une avec l'autre : contrairement
à l'entretien du système existant qui doit se faire avec des fonds
existants, les investissements visant à créer des richesses nouvelles
peuvent et dans certains cas *doivent* se faire par de la création
monétaire.

Ainsi lorsqu'on cherche à évaluer l'Action Publique - typiquement
pendant les élections nationales - c'est une erreur de ne discuter que
des problématiques de fiscalité et du budget. Certes il est utile de
débattre du montant et du mode de prélèvement des taxes, impôts et
autres cotisations sociales, ainsi que de la manière dont ces recettes
sont réparties entre des postes comme l'éducation, la recherche ou
l'armée; mais cela ne concerne que le maintien de l'activité existante.

Les politiques de création monétaire, au contraire, sont le reflet des
choix que nous faisons pour les dix, vingt ou trente années à venir, de
la direction dans laquelle nous souhaitons aller. C'est par ces
politiques que l'on oriente et que l'on réorganise continuellement le
travail humain, au fur et à mesure que de nouvelles forces productives
viennent à être disponibles – lorsque des avancées techniques permettent
de libérer du temps, des bras et des esprits. En l'occurence ce n'est
pas une petite mutation technologique que nous avons devant nous, avec
la robotisation massive de l'industrie et d'une partie des services. La
question se pose donc de savoir vers quoi réorienter les millions de
travailleurs qui seront mis en disponibilité : une surproduction de
biens matériels, une évolution qualitative du niveau de vie, le
développement d'activités apportant du sens comme la recherche et les
arts, ou l'explosion sociale.

Si l'on en parle si peu, c'est peut-être parce que ce levier de la
Création monétaire a été soustrait à la démocratie : en France où sont
écrites ces lignes et dans toute la zone euro, ce sont les banques
privées qui en sont les bénéficiaires quasi-exclusifs. La "Banque
Centrale Européenne" contrôle indirectement la quantité de nouvelle
monnaie qui sera mise en circulation à un moment donné (sous forme de
prêts à court ou moyen terme, et à travers des taux d'intérêts plus ou
moins élevés), mais ne regarde absolument pas ce qui sera fait avec. Son
dirigeant est par ailleurs indépendant des pouvoirs publics, il n'a de
consignes à recevoir ni de la population ni des élus. Dans un tel
système, ce sont les banques privées qui décident de *qui* va pouvoir
bénéficier de financements et pour *quel* projet, de *quelle* direction
à prendre pour le futur, et si cela va se faire avec plus ou moins
d'intervention publique. A quelques variations près c'est ainsi que cela
fonctionne dans les 15 pays les plus riches du monde, à l'exception
notable de l'Inde et de la Chine.

Il ne s'agit pas ici de faire le procès des banques privées pour leur
usage du pouvoir de création monétaire ; il s'agit d'abord de remettre
en cause ce système qui le leur délègue *entièrement*.

On entend de nombreux "experts" économiques, comme l'actuel président de
la Banque de France François Villeroy de Galhau, dire que les
gouvernement sont incapables de gérer la création monétaire, pour la
simple et bonne raison que les élus ne manqueront pas de "faire marcher
la planche à billet" comme si c'était un pouvoir magique leur permettant
de maquiller leur mauvaise gestion budgétaire. En France, les chiffres
de l'inflation ont plutôt tendance à démentir cette affirmation; et
d'ailleurs si l'on suivait ce raisonnement, on pourrait aussi bien se
demander si le président de la République est qualifié pour être le chef
des armées, ou si l'on ne devrait pas laisser le commandement de la
police à un "comité d'experts indépendants" plutôt qu'à des élus du
peuple. Mais l'on voudrait surtout que ces experts économistes nous
expliquent en quoi il est plus sain de financer le déficit public en
empruntant à des investisseurs privés, *moyennant un taux d'intérêt*,
plutôt que de le faire directement à la Banque centrale. Alors que les
recettes de l'Etat sont déjà amputées par la persistance du chômage de
masse, que le budget est alourdi par certains investissements
indispensables qui *auraient dû* être pris en charge par de la création
monétaire, l'on se retrouve à devoir payer des intérêts tout sauf
négligeables : en France en 2019, ils représentent la somme de 43
milliards d'euros, soit un huitième du budget de l'État et *près de la
moitié du déficit*, tandis que la somme des intérêts payés entre 1979 et
2018 dépasse les 1500 milliards d'euros – somme proche des deux tiers de
la production annuelle sur le territoire. Quant à la "relance
économique" et la remise en activité des travailleurs, l'État qui n'a
pas le gouvernail de la création monétaire n'a d'autre choix que
d'attirer coûte que coûte les investisseurs étrangers, moyennant des
concessions excessives vis-à-vis de juridictions douteuses comme les
paradis fiscaux, des niveaux de rémunération des actionnaires
incompatibles avec la bonne marche des entreprises, des logiques
d'allègement de charges, d'exonération fiscale, de réglementations sur
mesure et de privatisations qui entament encore plus ses moyens
d'actions... et participent d'une situation où 80% des richesses
générées mondialement reviennent à 1% de la population.

A tous ceux qui souhaitent un renouveau de la participation citoyenne
dans la vie politique, un profond changement de système, ou même une
nouvelle République, cette question du contrôle de la monnaie offre
ainsi une grande opportunité : celle de remettre un indispensable levier
de la construction humaine dans le champ du politique. Celle d'avoir
enfin les moyens de donner une direction à l'effort collectif, que ce
soit pour éradiquer la pauvreté, offrir des conditions de travail
décentes pour tous, construire une alternative à la société de
consommation et sa version digital, ou prendre à bras le corps les
questions écologiques. Celle d'avoir des hommes et des femmes politiques
qui peuvent faire ce pour quoi ils ont été élus, plutôt que de trouver
des moyens de sauver les apparences. Celle, éventuellement, de mettre
des représentants des forces productives et des catégories
socio-professionnelles dans la boucle de décision, par la constitution
d'un Conseil économique et social, car les différents corps de métiers
sont tout aussi bien placés que les parlementaires – parfois mieux –
pour savoir ce qui est souhaitable pour les entreprises.

Certes, il ne faut pas attendre des partis politiques majoritaires de se
positionner fermement en faveur d'un contrôle politique de la monnaie :
l'idée est encore peu ancrée dans le débat public, et la défendre ne
leur apporterait que beaucoup d'ennemis et trop peu d'alliés.

On n'a donc d'autre choix que de compter sur la population. Il faut
qu'un nombre croissant de citoyens prenne conscience de l'importance de
la création monétaire, et réalisent qu'il y a là un enjeu vital pour
l'avenir à donner à notre société.

C'est souvent là que le bât blesse, dirait-on, le citoyen moyen étant
d'ordinaire peu réceptif à ce genre de considération abstraite et
ennuyeuse. A quoi l'on pourrait répondre que les débats politiques
actuels sur la CSG, la taxe d'habitation ou le CICE sont tout aussi
abstraits, en plus de n'offrir que de maigres perspectives.

| Il est vrai que les problématiques de finances semblent éloignées de
  la vie de tous les jours, même si la monnaie est un objet des plus
  communs dans notre vie quotidienne.
| Il est vrai que la monnaie est souvent lié à la cupidité, au pouvoir
  et à la domination; ceux qui obéissent à ces penchants comprennent
  très bien comment l'employer à leurs fins, mais les autres peuvent
  n'en être que plus rebutés à l'idée d'en faire l'étude.

Il est vrai aussi que la monnaie est associée à un univers de chiffres,
d'objets comptables et mathématiques manquant de chair, bien loin du
concret de notre univers physique. Mais c'est là tout le problème :
l'argent *ne doit pas* rester déconnecté de l'organisation du travail
humain et du monde physique. Dans le cas contraire nous courons à la
catastrophe, car les effondrements monétaires ne manquent jamais de
provoquer des dommages similaires à ceux d'une guerre, voire une guerre
tout court comme nous l'avons vu au siècle dernier.

On dit volontiers que l'argent n'a pas d'odeur. En vérité il les a
toutes : celle du travail, celle du vol, celle de la paix, celle de la
guerre, celle du partage.

Il y en a une en particulier qu'il est bon d'apprendre à sentir :
l'odeur du futur.

C'est l'objet de l'article en trois parties qui va suivre.
