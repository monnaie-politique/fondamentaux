************************************
Pistes pour changer les institutions
************************************

Parce que la création monétaire permet d’organiser le travail humain, et
donc la manière dont l’Homme transforme son environnement ;

Parce qu’il n’y a pas qu’une seule manière d’envisager l’avenir, et
qu’on ne peut laisser cette question à un groupement d’experts ;

Parce qu’enfin, le bon fonctionnement du système monétaire est une
question de sécurité nationale ;

Au même titre que l’armée ou la police, la monnaie doit être dirigée par
les gouvernements.

#. Le mode de gouvernance

L’Institut d’Émission doit donc être sous tutelle politique, et doit
exécuter les décisions prises par les pouvoirs publics. S’il n’est pas
forcément nécessaire que son dirigeant soit élu, il doit avoir des
comptes à rendre et être révocable si besoin.

Sous quel mode doivent être décidées les grandes orientations de la
politique monétaire ? Plusieurs possibilités sont offertes. Nous
soulignons particulièrement la proposition de Pierre Mendès-France
s’inspirant de l’expérience de la France des années 45-60, où elles
étaient élaborées par des commissions rattachées à l’exécutif [1]_, et
prenaient finalement la forme d’un plan sur 5 ans qui devait être validé
par le parlement. Mendès-France proposait d’ajouter un « Conseil
Économique et Social » dans la boucle de décision. Celui-ci serait
composé de représentants des corps de métiers et forces vives du pays :
Les syndicats et les associations de consommateurs pourraient en faire
partie, et passeraient ainsi d’un rôle de revendication à un rôle de
décision dans les grandes orientations économiques  [2]_. Cette
triangulaire Exécutif – Parlement – Conseil économique, avec ses trois
modes de représentativité différents, permettrait que la faiblesse de
chacun soit équilibrée par celles des autres. Elle permettrait aussi de
garantir que l’ensemble des acteurs, ayant participé activement aux
décisions, soient pleinement mobilisés quant à leur mise en application.

|image0|\ Il sera ainsi possible de définir une politique de création
monétaire, qui nourrira les changements à prévoir ou à accompagner dans
l’activité humaine. Nous avons vu que le succès d’une telle intervention
nécessite une stratégie, une vue d’ensemble et une coordination entre
les acteurs, il faudra donc une certaine dose de planification. Pas
toutefois à la manière d’une procédure administrative à suivre à la
lettre, mais comme le ferait n’importe quelle grande entreprise qui
souhaite accomplir ses objectifs dans un environnement pas toujours
amical.

2. L’État

| La mise en place de ce circuit monétaire a de grandes conséquences sur
  l’action de l'État.
| Elle conduit en effet à redéfinir le rôle de la collecte de l’impôt,
  et de son usage qu’on appelle le « budget » : Ce dernier ne devrait en
  réalité être employé qu’au *maintien* de l’activité existante, et non
  à des investissements ayant pour but un *changement* dans l’activité ;
  ces derniers étant légitimement pris en charge par une création
  monétaire nouvelle. Dans les pays où l’Institut d’Émission a
  interdiction de financer les investissements publics (comme en Union
  Européenne), ces derniers sont souvent reportés sur le budget et
  creusent ainsi le déficit [3]_.
| Il faudrait en réalité définir trois types de revenus et de dépenses :

1. La collecte de l’impôt et le budget, qui doivent servir uniquement
aux affaires courantes et au maintien de ce qui existe déjà.

2. Des emprunts directs à l’Institut d’Émission, finançant les
investissements publics décidés dans le cadre de la stratégie économique
générale. Ces emprunts peuvent légitimement être consentis sans taux
d’intérêt. Ils doivent être remboursés, ce qui est la garantie que
l’investissement a été productif. Par contre cette forme de création
monétaire ne doit en aucun cas financer les affaires courantes, ce qui
pourrait provoquer une spirale inflationniste… en plus de donner raison
à tous ceux qui veulent maintenir l’indépendance de l’Institut
d’Émission, sous prétexte que l’État ne saura jamais s’en servir
correctement.

3. Ce que l’on pourrait nommer un « fonds de répartition », alimenté par
les créations de monnaie permanente que l’Institut d’Émission doit
réaliser en cas de croissance de l’activité. L’usage le plus évident de
cette monnaie nouvelle serait de la répartir entre tous les citoyens.
Néanmoins d’autres usages seraient tout aussi pertinents : indemniser
les laissés-pour-compte d’un changement technique, favoriser en priorité
l’achat des biens et services nouvellement développés, la réinvestir
dans des projets de modernisation énergétique. Ce fonds de répartition
peut aussi servir à rembourser les emprunts liés à certains
investissements publics, notamment lorsque leur rentabilité consiste en
des retombées économiques indirectes qui ne se mesurent que sur
l’activité globale (auquel cas d’ailleurs le secteur privé n’aurait pas
pu les prendre en charge). L’important est que les décisions concernant
l’usage de ces fonds soient liées à la stratégie d’ensemble et en
constituent la concrétisation.

Le respect de ces trois circuits permettra d’éviter les abus et les
mauvais usages de ce nouveau pouvoir de création monétaire. En dehors
des émissions monétaires liées à la croissance de l’activité économique,
*la planche à billet ne doit en aucun cas financer le déficit budgétaire
ou la dette publique*. Les partisans d’un Institut d’Émission gouverné
par des experts ont raison de souligner ce point, mais ils ont tort
lorsqu’ils affirment que les abus sont inévitables : si les garde-fous
sont connus et bien définis, un élu irresponsable ne pourra pas les
franchir ou sera immédiatement sanctionné.

Par ailleurs, l’absence d’émission de monnaie permanente au cours des
trente dernières années, (autre forme d’abus, mais cette fois-ci avérée
et commise par des « experts » !) devra être compensée par une création
monétaire massive, dont l’usage le plus immédiat serait un rachat des
crédits existants et donc une annulation des dettes.

Il est à noter par ailleurs que ces principes de création monétaires
sont très utiles dans le cadre de partenariats à l’étranger. Notamment
dans le cas de projets d’infrastructures lourds (chemins de fers,
centrales électriques, etc) ils permettent d’adjoindre à ces projets des
solutions de financement les rendant plus abordables, et ainsi de
toucher des pays n’ayant pas une capacité d’investissement suffisante
pour les acheter.

Outre qu'il permet d’équilibrer la balance commerciale et de constituer
des réserves en devises, cet atout est essentiel dans le monde
inter-connecté dans lequel nous vivons (la Chine en est un parfait
exemple) : à condition de savoir précisément ce que nous voulons en
échange, du type de relation que nous souhaitons établir, il permet
d’organiser la mondialisation plutôt que de la subir.

5. Le rôle des banques

Cette création monétaire doit-elle être assumée entièrement par
l’Institut d’Émission, ou doit-on en laisser une part aux banques ? Là
aussi il existe plusieurs possibilités.

Actuellement la création monétaire par le secteur bancaire est encadrée
par une réglementation *technique*, qui prévoit un ratio maximum entre
les fonds propres de la banque et le volume de crédit qu’elle peut
accorder. Ce ratio pourrait faire l’objet d’une décision *politique*,
liée aux objectifs à atteindre et au degré d’intervention souhaité des
pouvoirs publics dans la vie économique. À l’extrême il serait aussi
possible d’interdire complètement aux banques d’octroyer des crédits à
partir de leurs dépôts, et de donner un monopole de la création
monétaire à l’Institut d’Émission.

Il est aussi possible d’opérer avec des banques qui soient la propriété
de l’État, et donc au service de la stratégie d’ensemble. En Chine
aujourd’hui, comme en France dans les années 45-70, quatre grandes
banques sont nationalisées et ont la responsabilité chacune d’un secteur
de l’économie, permettant ainsi d’opérer de manière plus décentralisée.
Outre la force que cela donne à l’intervention publique, la présence de
banques nationalisées offre une possibilité importante au citoyen :
celle de mettre son épargne au service de la stratégie du gouvernement,
notamment s’il la préfère à celles des différentes banques privées.

Autre levier d’importance : la possibilité de fixer des critères précis
pour l’octroi de crédits, selon la stratégie définie et pour permettre
un certain degré de décentralisation. Il serait ainsi envisageable de
favoriser certains types d’entreprises en fonction de leur taille, de
leur adhésion à certaines valeurs, de la pertinence de leur contrôle
qualité ou de la durée de vie de leurs produits, de leur choix en termes
de rémunération du travail et du capital, etc. En France de tels
critères existaient jusqu’en 1980 sous la dénomination de « crédits
mobilisables ». Ces prêts accordés par les banques à des personnes
remplissant certains critères se voyaient rachetés automatiquement par
la Banque de France. Les banques étaient ainsi rémunérées pour jouer
l’intermédiaire entre l’État et le porteur de projet, et ce dernier
pouvait accéder plus facilement à financement soutenable.

Quelque soit l'option choisie, il convient de s'assurer de la stabilité
du système bancaire : la faillite d'un établissement gérant des milliers
de comptes de dépôts peut être dramatique. Les principales grandes
banques du monde le savent, et opèrent ainsi avec une garantie implicite
de l'État – chacun sait que la Réserve Fédérale américaine, la Banque
Centrale Européenne et les autres Instituts d'émission ne manqueront pas
d'ouvrir des lignes de crédit phénoménales pour éviter la
catastrophe. [4]_ La manière la plus simple et la plus efficace de
garantir cette stabilité est de restreindre les activités de la banque à
celles qui nous intéressent, à savoir gérer les dépôts et des accorder
des crédits. Les activités de spéculation et d'assurance seraient ainsi
transférées à des « banque d'investissement », dont la faillite aurait
un impact beaucoup plus limité sur l'activité économique, et qui ne
disposeraient pas de la force de frappe que constitue l’argent des
épargnants. Une telle séparation des métiers de banques permettrait de
remettre les choses à plat à la suite de la crise de 2008, dont nous
sentons toujours les effets, et d'éviter qu'elle puisse se reproduire.

Il nous reste deux aspects à évoquer, essentiels pour bien coordonner la
mise en place de ces politiques.

6. Note sur la mesure

| Il est essentiel de disposer des bons instruments de mesures,
  permettant de déterminer facilement les deux caractéristiques de notre
  flux monétaire que sont sa masse et sa vitesse de circulation. La
  première est directement contrôlée par l’Institut d’Émission ; pour la
  seconde nous disposons d’un indicateur bien connu, à savoir… le PIB ou
  Produit Intérieur Brut, qui traduit le prix de tout ce qui a été
  échangé sur notre territoire pendant une année. (ou la « valeur
  ajoutée » dans le cas de biens partiellement produits à l’étranger)
  Cet indicateur est donc très pertinent si l’on s’en sert pour avoir
  idée précise du flux monétaire, au lieu de le prendre comme référence
  de santé économique  [5]_.
| On peut noter que les monnaies basées sur la technologie blockchain et
  sur un fonctionnement décentralisé permettent d'être encore plus
  précis, le volume et l'historique des transactions étant lisibles en
  temps réel sur un livre de comptes accessible au grand public, avec
  toutefois les problèmes liés à une trop grande transparence, vis-à-vis
  d'acteurs malveillants notamment.

7. La véritable « stabilité des prix »

Enfin, nous avons vu que la stabilité des prix dans le domaine monétaire
est une des conditions de la prospérité économique. Si l'on veut traiter
cette question à fond, il n'est plus question de se contenter de réguler
le volume des crédits comme le fond les principales Banques Centrales :
*il faut réguler ou interdire la spéculation financière*.

Ce sujet mériterait un article au moins aussi long que celui-ci. Si vous
deviez toutefois en retenir que 3 choses, ce serait ceci :

Premièrement, certes la spéculation permet dans de nombreux cas de
réguler les prix (un peu comme congeler des fraises en été pour les
vendre en hiver), offre des moyens de financements aux grandes
entreprises à travers la bourse et donne les moyens aux producteurs de
se couvrir contre des variations désavantageuses du prix des
marchandises ou des monnaies. Elle peut donc être utile. Mais au regard
des proportions, ce serait une erreur de considérer que cela constitue
la fonction *principale* de la spéculation et que les bulles financières
sont un épiphénomène. Si l’on fait le bilan aujourd’hui, les
conséquences négatives de la spéculation sur l’activité économique sont
sans commune mesure avec ses quelques aspects positifs  [6]_.

Deuxièmement, l’argument consistant à dire que l’économie réelle a
besoin de la spéculation pour se financer (la fameuse « théorie du
ruissellement », qu’on pourrait aussi appeler la théorie des miettes) ne
tient que dans un système où la création monétaire ne joue pas son rôle.
Il contient donc peut-être une part de vérité aux États-Unis et en Union
Européenne aujourd’hui (où l’on se demande d’ailleurs si la création
monétaire ne sert pas tout court à spéculer) mais il sera faux dans le
système que nous cherchons à établir. Par ailleurs, la spéculation est
un jeu à somme nulle où il y a nécessairement un gagnant et un perdant,
et on ne peut donc en faire « ruisseler » que des richesses qui ont été
prises quelque part.

Troisièmement, il serait tout-à-fait légitime de fixer des prix
planchers pour certaines productions, afin de garantir que celles-ci ne
soient pas échangées en dessous du « prix naturel », notamment pour des
biens dont on ne peut contester la nécessité tels que les denrées
agricoles.

Si vous souhaitez aller un peu plus loin, plusieurs mesures simples
existent. L'une d'entre elles serait d’interdire la « vente à découvert
nue », pour faire simple l’achat/vente de produits de couvertures et
autres assurances sur des biens que l’on ne possède pas. (comme
s’assurer contre l’incendie de la maison du voisin et toucher de
l’argent si celle-ci brûle) Une mesure similaire pourrait être prise
concernant les échanges entre les monnaies, où selon la CADTM 95 % des
échanges de devises sont de type spéculatif. On pourrait restreindre les
mouvements de capitaux à ceux qui correspondent à une relation
commerciale existante ou en cours de constitution, et refuser les
mouvements purement spéculatifs.  [7]_ Mais, surtout, le plus important
est de cesser d'orienter la création monétaire vers la spéculation.

#. Conclusion

Certes le contrôle politique de la monnaie ne résout pas tout ; il
n’offre pas de solutions immédiates à l’approvisionnement énergétique,
aux problèmes de gestion et de corruption de l’État, aux conflits
géopolitiques de par le monde. Il ne dit pas comment une telle réforme
dans votre pays sera accueillie par les grandes puissances étrangères,
publiques et privées, et comment contenir l’agressivité des uns tout en
obtenant le soutien des autres. Il ne permet pas non plus de savoir si
la récolte sera bonne ou mauvaise l’année prochaine.

| Néanmoins il offre à l’intérêt général les moyens de s’exprimer. Il
  permet de sortir d'une situation où les problèmes s'accumulent non pas
  par manque de solutions, mais parce que celles-ci croupissent dans des
  cartons où l’austérité économique les a enfermées. Il offre un cadre
  où les grandes orientations pour le futur ne seront plus portées par
  quelques milliardaires visionnaires, mais partagées par l’ensemble de
  la population à travers les processus démocratiques.
| Il permet aussi de sortir d’une logique de démantèlement des États, où
  ces derniers n’ont aucun contrôle sur le crédit, sont dépendants des
  marchés financiers, et maintiennent un système qu'ils dirigent de
  moins en moins.

C'est en cela que nous disons que le contrôle politique de la monnaie
est vital : dans ce contrôle réside le choix entre développement et
effondrement économique, entre plein emploi et chômage de masse, entre
vision à long terme et un pillage court-termiste des ressources. *C'est
toute la différence entre une destinée collectivement choisie ou subie.*

Nous sommes conscients que face à de telles considérations sur
l’organisation des gouvernements, on peut se sentir comme face à un
vide. Comment réclamer un tel changement à nos hommes politiques ?
Peut-on vraiment intéresser la population à ces réflexions abstraites,
qui interviennent certes sur les causes mais sont loin des
problématiques quotidiennes ? Doit-on vraiment croire en ces conceptions
alors que, même si l’Histoire a eu plutôt tendance à les valider, elles
ne bénéficient encore que d’un faible ancrage universitaire ? Et,
typiquement dans le cas de l’Union Européenne, ne doit-on pas plutôt
trouver une autre solution pour colmater les problèmes immédiats,
sachant qu’un tel changement sur la monnaie, s’il devait être négocié
par les canaux habituels et avec les personnalités habituelles, ne
verrait pas le jour avant au moins une dizaine d’années ?

Ces questions ne trouveront pas de meilleure réponse que vos actions
personnelles et collectives. Vous pouvez vous faire vous-mêmes porteur
de cette idée et guetter chaque occasion de la transmettre, à commencer
par faire lire cet article. Si vous pensez que notre classe politique
est défaillante, vous pouvez tâcher d’en comprendre les raisons et
franchir vous-mêmes la barrière. Ii vous êtes professeur ou étudiant en
économie, poussez à fond cette idée de contrôle politique de la monnaie
et dans un sens ou dans l’autre, donnez-lui l’autorité qu’elle mérite.

*Enfin, ne sous-estimez pas l’importance d’une action collective
coordonnée.*

Dans tous les cas interrogez-vous véritablement sur les conséquences de
ce qui a été exposé ici, au-delà du simple objet d’étude intellectuelle.
Projetez-vous pleinement et intimement dans un monde où seraient mis en
place ces nouveaux moyens, puis dans un monde où nous laissons se
déployer les travers actuels du système.

Vous réaliserez sans doute que l’avenir nous offre de très belles
perspectives, mais que le temps nous est compté.

8. Pour aller plus loin

**Notions de base utilisées dans cet article**

| Théorie quantitative de la monnaie :
| https://www.lafinancepourtous.com/decryptages/politiques-economiques/theories-economiques/theorie-quantitative-de-la-monnaie/

| Loi de l’offre et de la demande :
| https://www.economie.gouv.fr/facileco/loffre-et-demande

**Sur le travail humain**

Vladimir Vernadski, *L’autotrophie de l’Humanité (1925)*
http://ddata.over-blog.com/xxxyyy/0/31/89/29/Fusion-108/F108.8.pdf

**L’économie physique et le lien avec la monnaie**

Lyndon LaRouche, *Alors, vous voulez tout savoir sur l’économie ?*
(1998), notamment les chapitres 4 et 7.

**La création monétaire dans le système actuel**

https://www.lafinancepourtous.com/decryptages/politiques-economiques/theories-economiques/creation-monetaire/la-creation-monetaire-comment-ca-marche/

https://www.lafinancepourtous.com/decryptages/marches-financiers/acteurs-de-la-finance/banque/la-banque-comment-ca-marche/

**Éléments sur le système à mettre en place**

Pierre Mendès France, *La république moderne* (1962), notamment les
chapitres 6 à 8.

Tovy Grjebine, *Récession et relance* (1984), notamment page 11-34 et
205.

Maurice Allais, *La crise mondiale aujourd’hui - pour de profondes
réformes du système financier international (1999)*

Proposition de loi 157 du 22 juillet 1981 :
https://www.agoravox.fr/actualites/politique/article/proposition-de-loi-157-du-22-187382

.. [1]
   Plus précisément, le commissariat au Plan dépendait directement du
   chef du gouvernement, et travaillait en tandem avec une « commission
   aux investissements » présidée par le ministre des Finances,
   comprenant le Commissaire général au Plan ou son délégué, le
   gouverneur de la Banque de France, les directeurs du Trésor, du
   Budget et des Programmes économiques ainsi que des représentants des
   ministères techniques, et chargée de déterminer par quel manière le
   plan allait être financé.

.. [2]
   La République Moderne » de Pierre Mendès-France, notamment les
   chapitres 5 à 8.

.. [3]
   C’est aussi le cas lorsque les investissements sont réalisées via un
   emprunt aux banques privées, car les intérêts sont pris en charge par
   le budget en tant que « service de la dette ». En France, ils
   représentent environ 40 milliards d’euro soit 10 % du budget total.

.. [4]
   Pour palier à cela, il a été décidé en Union Européenne en 2016
   qu'une partie des dépôts pouvaient être prélevés en cas de difficulté
   majeure de l'établissement. Cette politique avait précédemment été
   expérimentée à Chypre en 2013.

.. [5]
   Une erreur qui a conduit l'Union Européenne à inclure le trafic de
   drogue et la prostitution dans le calcul du PIB, ce qui a eu pour
   effet de gonfler les chiffres.

.. [6]
   La régulation des prix pourrait d’ailleurs tout aussi bien se faire
   avec une agence publique à but non lucratif et en constituant des
   stocks d’intervention.

.. [7]
   Un autre mesure, et qui avait d’ailleurs cours durant la période des
   accords de Bretton-Woods (1945-1970), était d’interdire aux banques
   d’avoir des comptes dans une devise étrangère. La première
   « entorse » à ce principe par les banques londoniennes à la fin des
   années 60 a permis par la suite la déstabilisation du dollar et a
   conduit à l’adoption d’un système à taux de changes flottants.

.. |image0| image:: organigramme.png
   :width: 7.77847in
   :height: 4.79861in
